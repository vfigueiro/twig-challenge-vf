# Group Maker

The `group_maker` module holds the `make_groups` function 
which splits a list of elements in the given number of groups.

The elements are distributed evenly across the groups 
leaving any compensation for unevenness in the last group.

The function accepts two arguments:
* `elements`: the list of elements to split, e.g. `[1, 2, 3, 4, 5]`
* `num_groups`: the number of groups to split the elements into, e.g. `3`

And return a list of lists (groups), e.g. `[[1, 2], [3, 4], [5]]`


## Requirements

* Python >= 3.9
* pytest and pytest-cov if you wish to run the tests


## Usage

Enter the Python shell while in this directory:

```bash
twig-challange-vf $ python
```

```python
from group_maker import make_groups

groups = make_groups(["a", "b", "c", "d", "e"], 3)

assert groups == [["a", "b"], ["c", "d"], ["e"]]
```


## Run the tests

```bash
twig-challange-vf $ poetry install
twig-challange-vf $ pytest --cov
```
