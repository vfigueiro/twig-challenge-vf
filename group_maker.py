from decimal import Decimal, ROUND_HALF_DOWN


def make_groups(elements: list, num_groups: int):
    """Split the given list of elements into <num_groups> groups where the last group holds the remainder

    Args:
        elements: a list of any number of elements
        num_groups: the number of groups to slit the elements into
    """
    num_elements = len(elements)

    # minimal argument validation
    if num_groups < 1:
        raise ValueError("The requested number of groups is less than one")
    if num_elements and num_groups > num_elements or not num_elements and num_groups > 1:
        raise ValueError("The requested number of groups is greater than the number of elements")

    # calculate the "regular" group size
    division_result = num_elements / num_groups
    # ensure that .5 is always rounded down
    group_size = int(Decimal(division_result).quantize(1, rounding=ROUND_HALF_DOWN))

    # build the groups
    groups = []
    for group_num in range(num_groups):
        start_pos = group_num * group_size
        at_last_group = group_num == num_groups - 1
        stop_pos = num_elements + 1 if at_last_group else start_pos + group_size
        groups.append(elements[start_pos:stop_pos])

    return groups
