# How and why this works

After ensuring that the arguments have acceptable values the algorithm establishes
how many elements a "regular group" - i.e. all except for the last one - will hold (GS).

It achieves this by dividing the number of elements by the requested number of groups
and rounding the result, to the unit, down until .5 (incl.) and up from .5.

It then iterates over the range of the requested number of groups, appending a slice
of GS elements to the result list for each group.

When the last group is reached, the algorithm appends any elements left, whose length can be
- less than GS if the rounding above occurred upwards
- equal to GS if the rounding above had no effect (perfectly even distribution)
- greater the GS if the rounding above occurred downwards

This ensures that every group holds as many elements as possible while leaving any
compensation for the last group.
