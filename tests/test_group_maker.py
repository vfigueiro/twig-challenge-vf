import pytest

from group_maker import make_groups


class TestMakeGroups:
    """Tests for the make_groups function"""

    NOT_POSITIVE_NUM_GROUPS_MSG = "The requested number of groups is less than one"
    TOO_FEW_ELEMENTS_MSG = "The requested number of groups is greater than the number of elements"

    def test_example_in_requirements(self):
        """Check that the example in the requirements doc works as expected"""
        groups = make_groups([1, 2, 3, 4, 5], 3)

        assert groups == [[1, 2], [3, 4], [5]]

    def test_less_elements_than_groups(self):
        """Passing less elements than the requested number of groups raises ValueError"""
        with pytest.raises(ValueError, match=self.TOO_FEW_ELEMENTS_MSG):
            make_groups([1], 2)

    def test_zero_num_groups(self):
        """Passing zero as num_groups raises ValueError"""
        with pytest.raises(ValueError, match=self.NOT_POSITIVE_NUM_GROUPS_MSG):
            make_groups([1], 0)

    def test_negative_num_groups(self):
        """Passing a negative num_groups raises ValueError"""
        with pytest.raises(ValueError, match=self.NOT_POSITIVE_NUM_GROUPS_MSG):
            make_groups([1], -1)

    def test_no_elements_more_than_one_group(self):
        """Passing an empty list and requesting more than one group raises ValueError"""
        with pytest.raises(ValueError, match=self.TOO_FEW_ELEMENTS_MSG):
            make_groups([], 2)

    def test_no_elements_one_group(self):
        """Returns an empty group when passing an empty list as elements"""
        groups = make_groups([], 1)

        assert groups == [[]]

    def test_one_element(self):
        """Returns a group with the single element when given only one"""
        groups = make_groups(["a"], 1)

        assert groups == [["a"]]

    def test_one_group(self):
        """Returns all elements in a single group"""
        groups = make_groups(["a", "b", "c"], 1)

        assert groups == [["a", "b", "c"]]

    def test_as_many_groups_as_elements(self):
        """Returns as many groups as elements"""
        groups = make_groups(["a", "b", "c"], 3)

        assert groups == [["a"], ["b"], ["c"]]

    def test_even_split(self):
        """Returns groups of even sizes"""
        groups = make_groups(["a", "b", "c", "d"], 2)

        assert groups == [["a", "b"], ["c", "d"]]

    def test_less_than_half(self):
        """Returns the expected groups when the calculated group_size is below half unit"""
        groups = make_groups(["a", "b", "c", "d", "e"], 4)

        assert groups == [["a"], ["b"], ["c"], ["d", "e"]]

    def test_at_half_even_number_down(self):
        """Returns the expected groups when the calculated group_size is half unit"""
        groups = make_groups(["a", "b", "c", "d", "e"], 2)

        assert groups == [["a", "b"], ["c", "d", "e"]]

    def test_at_half_even_number_up(self):
        """Returns the expected groups when the calculated group_size is half unit"""
        groups = make_groups(["a", "b", "c"], 2)

        assert groups == [["a"], ["b", "c"]]

    def test_at_half_even_number_up_also(self):
        """Returns the expected groups when the calculated group_size is half unit"""
        groups = make_groups(["a", "b", "c", "d", "e", "f", "g"], 2)

        assert groups == [["a", "b", "c"], ["d", "e", "f", "g"]]

    def test_more_than_half(self):
        """Returns the expected groups when the calculated group_size is above half unit"""
        groups = make_groups(["a", "b", "c", "d", "e"], 3)

        assert groups == [["a", "b"], ["c", "d"], ["e"]]
